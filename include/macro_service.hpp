#ifndef TASK_SERVICE_HPP
#define TASK_SERVICE_HPP

#include <algorithm>
#include <functional>
#include <string>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>
#include <chrono>

#include <fabui/wamp/backend_service.hpp>

namespace fabui {


	class MacroService: public BackendService {
		public:
			MacroService(const std::string& env_file);
			~MacroService();

		private:
			void onConnect();
			void onJoin();

			/* Service functions */
			void terminate_wrapper(wampcc::invocation_info info);
			//void terminate();

			void run_wrapper(wampcc::invocation_info info);
			bool run(const std::string& macro_name, const wampcc::json_object& env);

			void info_wrapper(wampcc::invocation_info info);
			bool info();
	};
}

#endif /* TASK_SERVICE_HPP */
