
/*
 * Copyright (c) 2017 Darren Smith
 *
 * wampcc is free software; you can redistribute it and/or modify
 * it under the terms of the MIT license. See LICENSE for details.
 */

#include "config.h"
#include <iostream>
#include <CLI/CLI.hpp>

#include "macro_service.hpp"

void show_version(size_t) {
	std::cout << PACKAGE_STRING << std::endl; 
	throw CLI::Success();
}

int main(int argc, char** argv)
{
	CLI::App app{PACKAGE_NAME};

	std::string uri   	 = "ws://127.0.0.1:9000";
	std::string realm 	 = "fabui";
	std::string	env_file = "/mnt/projects/Colibri-Embedded/fabui/fabui-frontend/application/.env";

	app.add_option("-u,--uri", uri, "WAMP default realm [default: " + uri + "]");
	app.add_option("-r,--realm", realm, "WAMP default realm [default: " + realm + "]");
	app.add_option("-e,--backend-env", env_file, "FABUI web .env file [default: " + env_file + "]");
	app.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
	app.add_flag_function("-v,--version", show_version, "Show version");

	try {
		app.parse(argc, argv);
	} catch (const CLI::ParseError &e) {
		return app.exit(e);
	}	
	
	try {
		
		fabui::MacroService taskservice(env_file);
		if( taskservice.connect(uri, realm)) {
			auto finished = taskservice.finished_future();
			finished.wait();
		}

	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return 1;
	}
}
