#include "config.h"
#include "macro_service.hpp"
#include <fabui/utils/string.hpp>
#include <iostream>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

MacroService::MacroService(const std::string& env_file)
	: BackendService(PACKAGE_STRING, env_file)
{

}

MacroService::~MacroService() {

}

void MacroService::onConnect() {
	join( realm(), {"token"}, "macro-service");
}

void MacroService::onJoin()
{
	std::cout << "Joined\n";
	provide("macro.service.terminate", std::bind(&MacroService::terminate_wrapper, this, _1));

	provide("macro.run", 	std::bind(&MacroService::run_wrapper, this, _1));
	provide("macro.info",	std::bind(&MacroService::info_wrapper, this, _1));
	
	std::cout << PACKAGE_NAME" started\n" << std::flush;
}

void MacroService::terminate_wrapper(wampcc::invocation_info info) {
	yield(info.request_id, {true});
	disconnect();
}

void MacroService::run_wrapper(wampcc::invocation_info info) {
	try {

		std::cout << "MACRO: " << info.args.args_list << std::endl;

		yield(info.request_id);
	} catch(const std::exception &e) {
		//std::lock_guard<std::mutex> lock(m_mutex_ws);
		//ws->invocation_error(info.request_id, "wamp.error.runtime_error", errors);
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

bool MacroService::run(const std::string& macro_name, const wampcc::json_object& env) {
	
}

void MacroService::info_wrapper(wampcc::invocation_info info) {
	json_object task_info = {
		{"type", "print"},
		{"controller", "make-print"}
	};

	yield(info.request_id, {}, task_info);
}

bool MacroService::info() {

}